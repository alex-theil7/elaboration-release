import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './src/views/HomeView'
import Search from './src/views/SearchPageView'
import TitleDetail from './src/views/TitleDetailView'
import Recommendations from './src/views/RecommendationView'

//Look towards this code and other files as an example to create
//Navigation is made possible through the React Navigation modules, look at the following documentation to understand
//https://reactnavigation.org/

//Create a Stack Navigator that allows the screen to connect
const Stack = createStackNavigator();

export default function App() {
  //Creates Navigation Container to allow Stack Navigation
  return (
    <NavigationContainer>
      <Stack.Navigator>

        <Stack.Screen
          name="Home"
          component={Home}
          options={{ title: 'Welcome' }}
        />
        <Stack.Screen name="Search" component={Search} />
        <Stack.Screen name="TitleDetail" component={TitleDetail} />
        <Stack.Screen name="Recommendations" component={Recommendations} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
