import firebase from "firebase/app";
import "firebase/firestore"

//Configuration for Firebase Application
var firebaseConfig = {
    apiKey: "AIzaSyDgy9WmHSY-z_feDm9apB76QrWoXfO9LgE",
    authDomain: "streambase-se.firebaseapp.com",
    projectId: "streambase-se",
    storageBucket: "streambase-se.appspot.com",
    messagingSenderId: "1056636737643",
    appId: "1:1056636737643:web:16d7cbe4921aca3c5eb92e",
    measurementId: "G-11JZ237GFG"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//Intialize firestore
const firestore = firebase.firestore()

const docRef = firestore.collection("users").doc("johnDoe");

export const getDocumentData = () => {
    return docRef.get().then((doc) => {
        if (doc.exists) {
            return doc.data()
        } else {
            console.log("No such document!");
        }
    }).catch((error) => {
        console.log("Error getting document:", error);
    });
}

//Update function will get the data and determine weather to remove or add the movie to the list
export const update = (id) => {
    //Get the data from the document
    getDocumentData()
        .then((data) => {
            //If the movie id already exist in the list, then remove it
            if (data.followedMovies.includes(id)) {
                //Allows to find the value and remove the movie id from the array
                return docRef.update({
                    followedMovies: firebase.firestore.FieldValue.arrayRemove(id)
                })
                    .then(() => {
                        alert("Removed from Firebase Follow List");
                    })
                    .catch((error) => {
                        // The document probably doesn't exist.
                        console.error("Error updating document: ", error);
                    });
            } else {
                //Allows to add the movie id to the array
                return docRef.update({
                    followedMovies: firebase.firestore.FieldValue.arrayUnion(id)
                })
                    .then(() => {
                        alert("Added to Firebase Follow List");
                    })
                    .catch((error) => {
                        // The document probably doesn't exist.
                        console.error("Error updating document: ", error);
                    });
            }
        })
}