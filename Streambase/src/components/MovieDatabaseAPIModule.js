const apiKey = "c4c81ee0662a6bb5b3b0495520d6c51a"

//This will send a request to the API to return a list of movies based on a specific keyword
export const getMovieList = (searchTerm) => {
    return fetch(`https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&query=${searchTerm}`)
    .then(data => data.json())
    .then(data => {
      return data;
    })
}

//This will send a request to the API to return the providers of the specific movie id given
export const getMovieProviders = (movieId) => {
  return fetch(`https://api.themoviedb.org/3/movie/${movieId}/watch/providers?api_key=${apiKey}`)
  .then(data => data.json())
    .then(data => {
      return data;
    })
}