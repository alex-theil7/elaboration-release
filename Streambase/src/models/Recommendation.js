export default class Recommendation {
    constructor(text, type) {
      this.text = text;
      this.type = type;
    }
  }