import {update} from '../components/FirebaseModule'

const Recommendation = require('../models/recommendation.js').default

let recommendation1 = new Recommendation ("Planet Hulk", "Movie");
let recommendation2 = new Recommendation ("Hulk: Where Monsters Dwell", "Movie");
let recommendation3 = new Recommendation ("Iron Man and Hulk: Heroes United", "Movie");

let recommendationList = [recommendation1, recommendation2, recommendation3]

export const updateUser = (id) => {
    update(id)
}
