import { getMovieList, getMovieProviders } from '../components/MovieDatabaseAPIModule'

//This will get the data from the API and return a filtered list
export const gatherMovieList = (queryString) => {
    return getMovieList(queryString)
        .then(data => {
            return filterContent(data)
        })
}

//Method will filter content and add the provider information
const filterContent = (movieList) => {
    let contentList = [];
    
    //For each movie in the movieList parameter
    movieList.results.forEach(movie => {
        let movieEntry = {}
        let movieDetails = [];
        let providers = []
        //Gets the providers with a specific movie id
        getMovieProviders(movie.id)
            .then(data => {
                //Checks to see if the certain property fields required exist before manipulating the data
                if (("results" in data)) {
                    if (("CA" in data.results)) {
                        if (("flatrate" in data.results.CA)) {
                            movieDetails = data.results.CA.flatrate
                            
                            movieEntry.id = movie.id
                            movieEntry.title = movie.original_title
                            movieEntry.overview = movie.overview
                            movieEntry.poster_url = movie.poster_path
                    
                            movieDetails.forEach(service => {
                                providers.push(service.provider_name)
                            });
                            movieEntry.providers = providers
                    
                            contentList.push(movieEntry)
                        }
                    }
                }
            })
    });
       
    return contentList
}