import { getDocumentData, update } from '../components/FirebaseModule'

export const updateUser = (id) => {
    update(id)
}

export const checkIfFollowingMovie = (id) => {
    return getDocumentData()
        .then((data) => {
            if (data.followedMovies.includes(id)) {
                return true;
            } else {
                return false;
            }
        }
        )
}