import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StatusBar } from 'expo-status-bar';

const Home = ({ navigation }) => {  
  return (
    <View style={styles.container}>
      <Text>Welcome to the Elaboration Release of Streambase</Text>
      <Text>Please click on any of the buttons to select the use case and find certain features that'll be improved in the future</Text>
      <StatusBar style="auto" />
      <Button
        title="Go to Search Screen"
        onPress={() =>
          navigation.navigate('Search')
        }
      />
      <Button
        title="Go to Title Details Screen (Follow List Adding/Removal)"
        onPress={() =>
          navigation.navigate('TitleDetail')
        }
      />

      <Button
        title="Go to Recommendations Screen"
        onPress={() =>
          navigation.navigate('Recommendations')
        }
      />


    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default (Home)
