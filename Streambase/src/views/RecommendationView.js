import { StyleSheet, Text, View, Button, Alert } from 'react-native';
import { updateUser } from '../viewmodels/RecommendationPanelViewModel';
import React, { useState } from 'react';


const Recommendations = ({ navigation }) => {


return (
    <View style={styles.container}>
      

      <Text style={{ fontSize: 50, font: 'Roboto', textAlign: 'center', color: 'white', margin: 5 }}>Your Recommendations:</Text>
      <Text style={{ fontSize: 50, font: 'Roboto', textAlign: 'center', color: 'white', margin: 5 }}>Planet Hulk</Text>
      <Text style={{ fontSize: 15, font: 'Roboto', textAlign: 'center', color: 'gray', margin: 5 }}>Movie</Text>

      
      <View style={{ marginBottom: 2, marginTop: 10 }}>
        <Button
          title="View Recommendation Details"
        />
      </View>

    <View style={{ marginBottom: 2, marginTop: 2 }}>
      <Button
          title="Follow Recommendation"
          onPress={() => {
            updateUser("DEF567")
          }
        }
        />
      </View>

      <View style={{ marginBottom: 2, marginTop: 2 }}>
        <Button
          title="Deny Recommendation"

          
        />
      </View>


      <Text style={{ fontSize: 50, font: 'Roboto', textAlign: 'center', color: 'white', margin: 5 }}>"Hulk: Where Monsters Dwell"</Text>
      <Text style={{ fontSize: 15, font: 'Roboto', textAlign: 'center', color: 'gray', margin: 5 }}>Movie</Text>

      
      <View style={{ marginBottom: 2, marginTop: 10 }}>
        <Button
          title="View Recommendation Details"
        />
      </View>

    <View style={{ marginBottom: 2, marginTop: 2 }}>
      <Button
          title="Follow Recommendation"
          onPress={() => {
            updateUser("CDF123")
          }
        }
        />
      </View>

      <View style={{ marginBottom: 2, marginTop: 2 }}>
        <Button
          title="Deny Recommendation"

        />
      </View>

      <Text style={{ fontSize: 50, font: 'Roboto', textAlign: 'center', color: 'white', margin: 5 }}>"Iron Man and Hulk: Heroes United"</Text>
      <Text style={{ fontSize: 15, font: 'Roboto', textAlign: 'center', color: 'gray', margin: 5 }}>Movie</Text>

      
      <View style={{ marginBottom: 2, marginTop: 10 }}>
        <Button
          title="View Recommendation Details"
        />
      </View>

    <View style={{ marginBottom: 2, marginTop: 2 }}>
      <Button
          title="Follow Recommendation"
          onPress={() => {
            updateUser("KHJ567")
          }
        }
        />
      </View>

      <View style={{ marginBottom: 2, marginTop: 2 }}>
        <Button
          title="Deny Recommendation"

        />
      </View>

      
    </View>
  );
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#000000',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  
  export default (Recommendations)