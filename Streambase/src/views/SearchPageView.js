import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity} from 'react-native';
import { gatherMovieList } from '../viewmodels/SearchPageViewModel'



const Search = ({ navigation }) => {
  const [queryString, onChangeQueryString] = useState("")
  const [movieList, setMovieList] = useState()
  
  //This will wait for the function to occur then log the data returned from the viewmodel
  const sendMovieList = () => {
    if (queryString == ""){
      alert("Please Enter something in the textbox")
    }
    else {
      gatherMovieList(queryString)
        .then(data => {
          console.clear()
          alert("The text was sent and the data was recived, take a look at the inspection tool and under the console tab to see the data that was recived.")
          console.log(data)
          setMovieList(data)
        })
    }
  }

  return (
    <View style={styles.container}>
      <Text>Search Screen</Text>
      <View style={styles.searchContainer}>
        <TextInput
          onChangeText={onChangeQueryString}
          value={queryString}
          style={styles.textInput}
          placeholder="Please Enter Search Keyword"
        />
        <TouchableOpacity 
          style={styles.searchBtn}
          onPress={() => {sendMovieList()}}
        >  
          <Image 
            source={{uri: "https://www.seekpng.com/png/full/920-9209972_magnifying-glass-png-white-search-icon-white-png.png"}}
            style={{width: 35, height: 35}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000',
  },
  searchContainer: {
    flexDirection: 'row',
  },
  textInput: {
    backgroundColor: "#fff",
    margin: 25,
    padding: 15,
    borderRadius: 5,
    flex: 20
  },
  searchBtn: {
    backgroundColor: '#2196F3',
    borderRadius: 15,
    margin: 10,
    padding: 20,
    flex: 1,
  }
});

export default (Search)
