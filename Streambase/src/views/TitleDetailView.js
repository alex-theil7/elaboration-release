import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, Image, Linking, Alert } from 'react-native';
import { checkIfFollowingMovie, updateUser } from '../viewmodels/TitleDetailViewModel'

const TitleDetail = ({ navigation }) => {

  const [text, setText] = useState(checkIfFollowingMovie("ABC123") ? "+ Add to Follow List" : "- Remove from Follow List")

  const updateUserAndText = (id) => {
    updateUser(id)

    if (text == "- Remove from Follow List") {
      setText("+ Add to Follow List")
    } else {
      setText("- Remove from Follow List")
    }
  }

  return (
    <View style={styles.container}>
      <Image source={{ uri: 'https://bit.ly/3x51CgS' }} style={{ width: 1000, height: 300, borderRadius: 30 }} />

      <Text style={{ fontSize: 50, font: 'Roboto', textAlign: 'center', color: 'white', margin: 5 }}>Spider-Man: Into the Spider-Verse (2018)</Text>
      <Text style={{ fontSize: 20, font: 'Roboto', textAlign: 'center', color: 'white', margin: 5 }}>Miles Morales is juggling his life between being a high school student and being a spider-man. When Wilson "Kingpin" Fisk uses a super collider, others from across the Spider-Verse are transported to this dimension.</Text>
      <Text style={{ fontSize: 15, font: 'Roboto', textAlign: 'center', color: 'gray', margin: 5 }}>Available on: Disney+, Netflix, Prime Video</Text>

      <View style={{ marginBottom: 10, marginTop: 10 }}>
        <Button
          title="▶ Play"
          onPress={
            () => {
              Linking.openURL("https://bit.ly/3v1nKH3").catch(err => console.error("Couldn't load page", err));
            }
          }
        />
      </View>

      <View>
        <Button
          title={text}

          onPress={() => {
            updateUserAndText("ABC123")
          }
          }
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default (TitleDetail)